﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebApiCoreAuthenticationCenter.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AuthenticationController : ControllerBase
    {
        private ILogger<AuthenticationController> _logger = null;
        private IJWTService _iJWTService = null;
        private readonly IConfiguration _iConfiguration;
        public AuthenticationController(ILoggerFactory factory, ILogger<AuthenticationController> logger, IConfiguration configuration, IJWTService service)
        {
            this._logger = logger;
            this._iConfiguration = configuration;
            this._iJWTService = service;
        }

        [HttpGet]
        public IEnumerable<int> Get()
        {
            return new List<int> { 1, 2, 3, 5, 6, 7 };
        }

        [HttpGet]
        public JsonResult Login(string name, string password)
        {
            //去数据库校验登录信息
            if ((name ?? "").Length > 5 && string.Equals("123456", password))
            {
                string token = this._iJWTService.GetToken(name);
                return new JsonResult(new { success = true, token });
            }
            return new JsonResult(new { success = false, token = "" });
        }
    }
}